---
layout: post
title:  "Cold brew Coffee ala Genovese"
description: My personal recipe for a coffee lover summer must
date:   2019-08-01 10:00:00 +0100
categories: food drinks summer coffee recipes
---
Ahhh! Summertime. And the living is easy.
Well.. not so much. Hot weather can be quite unforgiving if you suffer from it, like I do.

For me, summer comes with a conflicting paradox:

1. I love coffee. I cannot live without it - yeah, I know. I can, but for the sake of the argument, follow my lead here.
2. I hate being hot and sweating.
3. If I drink hot coffee, I start to get hot and sweat.

So, in order to solve this paradox, how about if we go for a **Cold Brew**?

Cold Brew coffee is about infusing the gound coffee in cold or room temperature water and let it rest for a long period of time.
There is no other secret to it.

After much experimenting, I came up with my favourite recipe which I'm glad I can share it with you.

### Ingredients
* 110 grams of coffee beans
* 1 liter of filtered water
* 8 pods of Cardamome

### Steps
1. Open the cardamome pods and extract the seeds
2. Coarsely ground the coffee and the cardamome seeds together
3. Fill a jar or pitcher with the filtered water
4. Put the ground coffee and cardamome seeds in the water
5. Mix thoroughly with a spoon
7. Make sure that the jar or pitcher is covered by something, even a cloth with an elastic band will do.
8. Let the mix rest at room temperature for 8 hours
9. Mix the water again with a spoon
10. Put the mix in the fridge for another 8 hours or more
11. Filter the coffee with a french press or similar

### How to serve it
My favorite way is:
1. In a 200 cm3 glass, add 4 medium sized ice cubes
2. Fill it to 3/4 with the cold bew
3. Add a thin slice of lemon
4. Complete it with filtered water